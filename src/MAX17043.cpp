/*
MAX17043.cpp - MAX17043 class
Copyright (C) 2015 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <Wire.h>
#include "MAX17043.h"

uint16_t MAX17043::_read_reg(Register reg) {
  Wire.beginTransmission(MAX17043_ADDRESS);
  Wire.write(static_cast<uint8_t>(reg));
  Wire.endTransmission();

  _error = false;
  if (Wire.requestFrom(MAX17043_ADDRESS, (size_t)2) != 2) {
    _error = true;
    return 0;
  }

  int high = Wire.read();
  if (high == -1) {
    _error = true;
    return 0;
  }

  int low = Wire.read();
  if (low == -1) {
    _error = true;
    return 0;
  }

  return (high << 8) | low;
}

void MAX17043::_write_reg(Register reg, uint16_t val) {
  _error = false;
  Wire.beginTransmission(MAX17043_ADDRESS);
  if (Wire.write(static_cast<uint8_t>(reg)) == 0) {
    _error = true;
    return;
  }
  if (Wire.write(val >> 8) == 0) {
    _error = true;
    return;
  }
  if (Wire.write(val & 0xff) == 0) {
    _error = true;
    return;
  }
  Wire.endTransmission();
}

MAX17043::MAX17043() :
  _error(false)
{
  _config = _read_reg(Register::CONFIG);
}

float MAX17043::Vcell(void) {
  return static_cast<float>(_read_reg(Register::VCELL) >> 4) * 0.00125;
}

float MAX17043::SoC(void) {
  return static_cast<float>(_read_reg(Register::SOC)) * 0.00390625;
}

uint16_t MAX17043::version(void) {
  return _read_reg(Register::VERSION);
}

uint8_t MAX17043::alert_threshold(void) const {
  return (~_config & static_cast<uint16_t>(Config_mask::ATHD)) + 1;
}

MAX17043& MAX17043::set_alert_threshold(uint8_t val) {
  val = ~(val - 1) & static_cast<uint16_t>(Config_mask::ATHD);
  _config &= ~static_cast<uint16_t>(Config_mask::ATHD);
  _config |= val;

  return *this;
}

bool MAX17043::alert_flag(void) {
  _config = _read_reg(Register::CONFIG);
  return _config & static_cast<uint16_t>(Config_mask::ALRT);
}

bool MAX17043::sleep(void) const {
  return _config & static_cast<uint16_t>(Config_mask::SLEEP);
}

MAX17043& MAX17043::set_sleep(bool val) {
  if (val)
    _config |= static_cast<uint16_t>(Config_mask::SLEEP);
  else
    _config &= ~static_cast<uint16_t>(Config_mask::SLEEP);

  return *this;
}

uint8_t MAX17043::rcomp(void) const {
  return (_config & static_cast<uint16_t>(Config_mask::RCOMP)) >> static_cast<uint8_t>(Config_shift::RCOMP);
}

MAX17043& MAX17043::set_rcomp(uint8_t val) {
  _config &= ~static_cast<uint16_t>(Config_mask::RCOMP);
  _config |= static_cast<uint16_t>(val) << static_cast<uint8_t>(Config_shift::RCOMP);

  return *this;
}

void MAX17043::write_config(void) {
  _write_reg(Register::CONFIG, _config);
}

void MAX17043::reset(void) {
  _write_reg(Register::COMMAND, static_cast<uint16_t>(Command::POR));
}

void MAX17043::quickStart(void) {
  _write_reg(Register::MODE, static_cast<uint16_t>(Mode_Command::Quick_Start));
}
