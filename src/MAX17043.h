/*
MAX17043.h - MAX17043 class
Copyright (C) 2015 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once
#include <stdint.h>

#define MAX17043_ADDRESS 0x36

class MAX17043 {
private:
  enum class Register : uint8_t {
    VCELL	= 0x02,	// R/O
    SOC		= 0x04,	// R/O
    MODE	= 0x06,	// W/O
    VERSION	= 0x08,	// R/O
    __REG5	= 0x0a,
    CONFIG	= 0x0c,	// R/W

    COMMAND	= 0xfe,	// W/O
  };

  enum class Mode_Command : uint16_t {
    Quick_Start	= 0x4000,
  };

  enum class Config_mask : uint16_t {
    ATHD	= 0x001f,
    ALRT	= 0x0020,

    SLEEP	= 0x0080,
    RCOMP	= 0xff00,
  };

  enum class Config_shift : uint8_t {
    ATHD	= 0,
    ALRT	= 5,
    SLEEP	= 7,
    RCOMP	= 8,
  };

  enum class Command : uint16_t {
    POR		= 0x0054,
  };

  bool _error;
  uint8_t _config;

  uint16_t _read_reg(Register reg);
  void _write_reg(Register reg, uint16_t val);

public:
  MAX17043();

  bool error(void) const { return _error; }

  // Cell voltage in volts
  float Vcell(void);

  // State of charge in percent
  float SoC(void);

  uint16_t version(void);

  uint8_t alert_threshold(void) const;
  MAX17043& set_alert_threshold(uint8_t val);

  // Read-only, forces CONFIG to be read
  bool alert_flag(void);

  bool sleep(void) const;
  MAX17043& set_sleep(bool val);

  // WTF is RCOMP?!?
  uint8_t rcomp(void) const;
  MAX17043& set_rcomp(uint8_t val);

  // Write CONFIG register
  void write_config(void);

  void reset(void);
  void quickStart(void);

};
